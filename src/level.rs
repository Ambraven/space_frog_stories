use std::fs;

use bevy::prelude::*;
use serde::Deserialize;

use crate::{tileset::TileSetInfo, utils::{Collider, SCALE}};

#[derive(Deserialize, Debug)]
pub struct LevelInfo {
    pub compressionlevel: i8,
    pub height: usize,
    pub infinite: bool,
    pub layers: Vec<LayerInfo>,
    pub nextlayerid: usize,
    pub nextobjectid: usize,
    pub orientation: String,
    pub renderorder: String,
    pub tiledversion: String,
    pub tileheight: usize,
    pub tilesets: Vec<TileSet>,
    pub tilewidth: usize,
    pub version: String,
    pub width: usize
}

#[derive(Deserialize, Debug)]
pub struct LayerInfo {
    pub data: Vec<usize>,
    pub height: usize,
    pub name: String,
    pub opacity: u8,
    pub visible: bool,
    pub width:usize,
    pub x: usize,
    pub y: usize
}

#[derive(Deserialize, Debug)]
pub struct TileSet {
    pub firstgid: usize,
    pub source: String
}

impl LevelInfo {
    pub fn new(file: &str) -> Self {
        let content = fs::read_to_string(file).expect(&format!("impossible to read {}", file));

        serde_json::from_str(&content).expect(&format!("couldn't parse {}", file))
    }
}

pub fn load_level(
    mut cmds: Commands,
    assets: Res<AssetServer>,
    mut tiles_sheet: ResMut<Assets<TextureAtlas>>,
    level: &str
) {
    let level_info = LevelInfo::new(&format!("assets/sprites/{level}.json"));
    let tile_set_name = level_info.tilesets.first().unwrap().source.split(".").next().unwrap();
    let tile_set_info = TileSetInfo::new(&format!("assets/sprites/{tile_set_name}.json"));
    let tiles_size = Vec2::new(tile_set_info.tilewidth as f32, tile_set_info.tileheight as f32);
    let padding = Vec2::new(tile_set_info.spacing as f32, tile_set_info.spacing as f32);
    let offset = Vec2::new(tile_set_info.margin as f32, tile_set_info.margin as f32);
    let tiles_sheet_image = assets.load(&format!("sprites/{}", tile_set_info.image));
    let atlas = TextureAtlas::from_grid(
        tiles_sheet_image,
        tiles_size,
        tile_set_info.columns,
        tile_set_info.tilecount / tile_set_info.columns,
        Some(padding),
        Some(offset));
    let tiles_sheet_handle = tiles_sheet.add(atlas);

    for (layer_level, layer) in level_info.layers.iter().enumerate() {
        for (tile_pos, tile_id) in layer.data.iter().enumerate() {
            spawn_tile(&mut cmds, tiles_sheet_handle.clone(), layer, layer_level, *tile_id, tile_pos);
        }
    }
}

pub fn spawn_tile(
    cmds: &mut Commands,
    tiles_sheet: Handle<TextureAtlas>,
    layer: &LayerInfo,
    layer_level: usize,
    tile_id: usize,
    tile_pos: usize
) {
    if tile_id == 0 {
        return;
    }

    let tile_size_xy = 16;
    let layer_width_px = tile_size_xy * layer.width;
    let layer_height_px = tile_size_xy * layer.height;
    let layer_orig_x = -((layer_width_px / 2) as f32);
    let layer_orig_y = -((layer_height_px / 2) as f32);
    let tile_x = ((tile_pos % layer.width) * tile_size_xy) as f32 + layer_orig_x;
    let tile_y = ((tile_pos / layer.width) * tile_size_xy) as f32 + layer_orig_y;

    let mut tile = cmds.spawn(
        SpriteSheetBundle {
            texture_atlas: tiles_sheet,
            sprite: TextureAtlasSprite::new(tile_id - 1),
            transform: Transform{
                scale: Vec3::splat(SCALE),
                translation: Vec3::new(tile_x * SCALE, -tile_y * SCALE, layer_level as f32),
                ..Default::default()
            },
            ..Default::default()
        }
    );
    if layer.name == "walls" {
        tile.insert(Collider);
    }
}