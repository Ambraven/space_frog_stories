use bevy::prelude::*;

#[derive(Component, Deref, DerefMut)]
pub struct AnimationTimer(pub Timer);


#[derive(Component, Debug)]
pub struct AnimationIndices {
    pub first: usize,
    pub last: usize
}

#[derive(Component)]
pub struct Moving;

#[derive(Component)]
pub struct CharacterSprite {
    pub walk_down_handle: Handle<TextureAtlas>,
    pub walk_up_handle: Handle<TextureAtlas>,
    pub walk_left_handle: Handle<TextureAtlas>,
    pub walk_right_handle: Handle<TextureAtlas>,
}

#[derive(Component)]
pub struct Collider;

pub const SCALE:f32 = 3.0;