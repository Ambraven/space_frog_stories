use bevy::prelude::*;

mod player;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use player::*;
mod utils;
use utils::*;
mod tileset;
mod level;
use level::*;

fn main() {

    App::new()
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest()))
        .add_plugins(WorldInspectorPlugin::new())
        .add_systems(Startup, (setup, setup_player))
        .add_systems(Update, player_control)
        .add_systems(PostUpdate, animate_sprites)
        .run();
}

fn setup(
    cmds: Commands,
    asset_server: Res<AssetServer>,
    texture_atlases: ResMut<Assets<TextureAtlas>>
) {
    load_level(cmds, asset_server, texture_atlases, "frog_island");
}

fn animate_sprites(
    time : Res<Time>,
    mut query: Query<(
        &AnimationIndices,
        &mut AnimationTimer,
        &mut TextureAtlasSprite
    ), With<Moving>>
) {
    for (indices, mut timer, mut sprite) in query.iter_mut() {
        timer.tick(time.delta());
        if timer.just_finished() {
            sprite.index = if sprite.index == indices.last {
                indices.first
            } else {
                sprite.index + 1
            };
        }
    }
}