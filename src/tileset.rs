use serde::Deserialize;
use std::fs;

#[derive(Deserialize, Debug)]
pub struct TileSetInfo {
    pub columns: usize,
    pub image: String,
    pub imageheight: usize,
    pub imagewidth: usize,
    pub margin: usize,
    pub name: String,
    pub spacing: usize,
    pub tilecount: usize,
    pub tiledversion: String,
    pub tileheight: usize,
    pub tilewidth: usize,
    pub version: String,
}

impl TileSetInfo {
    pub fn new(file: &str) -> Self {
        let content = fs::read_to_string(file).expect(&format!("impossible to read {}", file));

        serde_json::from_str(&content).expect(&format!("couldn't parse {}", file))
    }
}