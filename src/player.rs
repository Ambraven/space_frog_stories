use bevy::{prelude::*, sprite::collide_aabb::{collide, Collision}};

use crate::utils::*;

#[derive(Component)]
pub struct Player;

pub fn setup_player(
    mut cmds: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>
) {
    let texture_handle = asset_server.load("sprites/character.png");
    let tile_size = Vec2::new(16.0, 22.0);
    let down_atlas = TextureAtlas::from_grid(texture_handle.clone(), tile_size, 4, 1, None, Some(Vec2::new(0.0, 6.0)));
    let walk_down_handle = texture_atlases.add(down_atlas);
    let right_atlas = TextureAtlas::from_grid(texture_handle.clone(), tile_size, 4, 1, None, Some(Vec2::new(0.0, 38.0)));
    let walk_right_handle = texture_atlases.add(right_atlas);
    let up_atlas = TextureAtlas::from_grid(texture_handle.clone(), tile_size, 4, 1, None, Some(Vec2::new(0.0, 70.0)));
    let walk_up_handle = texture_atlases.add(up_atlas);
    let left_atlas = TextureAtlas::from_grid(texture_handle.clone(), tile_size, 4, 1, None, Some(Vec2::new(0.0, 102.0)));
    let walk_left_handle = texture_atlases.add(left_atlas);

    cmds.spawn(Camera2dBundle::default());
    cmds.spawn((
        SpriteSheetBundle {
            texture_atlas: walk_down_handle.clone(),
            sprite: TextureAtlasSprite::new(0),
            transform: Transform{
                scale: Vec3::splat(SCALE),
                translation: Vec3::new(-32.0, 0.0, 10.0),
                ..Default::default()
            },
            ..default()
        },
        AnimationIndices{first: 0, last: 3},
        AnimationTimer(Timer::from_seconds(0.2, TimerMode::Repeating)),
        CharacterSprite{walk_down_handle, walk_right_handle, walk_left_handle, walk_up_handle},
        Player,
    ));
}

pub const PLAYER_SPEED: f32 = 100.0;

pub fn player_control(
    mut cmds: Commands,
    time: Res<Time>,
    keyboard : Res<Input<KeyCode>>,
    mut player_query: Query<(Entity, &mut Transform, &mut Handle<TextureAtlas>, &mut TextureAtlasSprite, &CharacterSprite, Option<&Moving>), With<Player>>,
    collider_query: Query<&Transform, (With<Collider>, Without<Player>)>
) {
    for (entity, mut transform, mut texture_altas, mut sprite, character_atlases, moving) in &mut player_query {
        let mut direction: Vec3 = Vec3::new(0.0, 0.0, 0.0);
        if keyboard.pressed(KeyCode::Up) {
            direction.y += 1.0;
            *texture_altas = character_atlases.walk_up_handle.clone();
            
        }
        if keyboard.pressed(KeyCode::Down) {
            direction.y -= 1.0;
            *texture_altas = character_atlases.walk_down_handle.clone();
        }
        if keyboard.pressed(KeyCode::Right) {
            direction.x += 1.0; 
            *texture_altas = character_atlases.walk_right_handle.clone();
        }
        if keyboard.pressed(KeyCode::Left) {
            direction.x -= 1.0;
            *texture_altas = character_atlases.walk_left_handle.clone();
        }

        if direction.length() != 0.0 {
            let old_pos = transform.translation;
            transform.translation += direction.normalize() * PLAYER_SPEED * time.delta_seconds();
            
            for collider in &collider_query {
                if let Some(collision) = collide(
                    transform.translation,
                    Vec2::new(16.0 * SCALE, 16.0 * SCALE),
                    collider.translation,
                    Vec2::new(16.0 * SCALE, 16.0 * SCALE)
                ) {
                    println!("Collision !! {:?}", collision);
                    match collision {
                        Collision::Bottom => if direction.y >= 0.0 { transform.translation.y = old_pos.y; } 
                        Collision::Top => if direction.y <= 0.0 { transform.translation.y = old_pos.y;  }
                        Collision::Left => if direction.x >= 0.0 { transform.translation.x = old_pos.x;  }
                        Collision::Right => if direction.x <= 0.0 { transform.translation.x = old_pos.x; }
                        _ => {}
                    }
                }
            }
            
            if moving.is_none() {
                cmds.entity(entity).insert(Moving);
                sprite.index = 1;
            }
        } else {
            if moving.is_some() {
                cmds.entity(entity).remove::<Moving>();
                sprite.index = 0;
            }
        }
    }
}